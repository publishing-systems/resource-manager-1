<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/upload.lang.php
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */



define("LANG_PAGETITLE", "Upload");
define("LANG_HEADER", "Upload");
define("LANG_BUTTONCAPTION_SEND", "Send");
define("LANG_TEXT_FILENAME", "Suggested file name (leave empty to keep the original file name)");
define("LANG_OPTIONCAPTION_DOWNLOADTYPEONCE", "once");
define("LANG_OPTIONCAPTION_DOWNLOADTYPEALWAYS", "always");
define("LANG_TEXT_SUBMITERROR", "Submission erroneous.");
define("LANG_TEXT_ERRORNUMBER", "Error occurred with number ");
define("LANG_TEXT_FILETOOLARGE", "File has more bytes than ");
define("LANG_TEXT_ERRORFILEMOVE", "The file has been successfully uploaded, but couldn’t be stored.");
define("LANG_TEXT_ERRORDBINSERTFILE", "An error occurred while inserting the entry into the database.");
define("LANG_TEXT_SUCCESSUPLOAD", "Upload successful!");
define("LANG_LINKCAPTION_UPLOADNEXT", "Upload another file");
define("LANG_LINKCAPTION_FILES", "Files overview");



?>
