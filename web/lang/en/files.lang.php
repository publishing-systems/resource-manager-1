<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/files.lang.php
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */



define("LANG_PAGETITLE", "Files");
define("LANG_HEADER", "Files");
define("LANG_LINKCAPTION_UPLOAD", "Upload");
define("LANG_TABLECOLUMNCAPTION_FILENAME", "Name");
define("LANG_TABLECOLUMNCAPTION_URL", "URL");
define("LANG_TABLECOLUMNCAPTION_DOWNLOADTYPE", "Type");
define("LANG_TABLECOLUMNCAPTION_CREATED", "Created");
define("LANG_TABLECOLUMNCAPTION_DOWNLOADED", "Downloaded");
define("LANG_TEXT_DOWNLOADTYPEONCE", "once");
define("LANG_TEXT_DOWNLOADTYPEALWAYS", "always");



?>
