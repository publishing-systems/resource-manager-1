<?php
/* Copyright (C) 2014-2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/de/index.lang.php
 * @author Stephan Kreutzer
 * @since 2014-05-31
 */



define("LANG_PAGETITLE", "Ressourcenverwaltung");
define("LANG_HEADER", "Ressourcenverwaltung");
define("LANG_INSTALLBUTTON", "Installieren");
define("LANG_INSTALLDELETEFAILED", "Die Installation wurde bereits erfolgreich durchgeführt, jedoch ist es der Installation nicht gelungen, sich selbst zu löschen. Bitte löschen Sie mindestens die Datei <tt>\$/install/install.php</tt> oder gleich das ganze Verzeichnis <tt>\$/install/</tt> manuell.");
define("LANG_WELCOMETEXT", "Willkommen bei unserer Ressourcenverwaltung!");
define("LANG_LOGINDESCRIPTION", "Anmeldung:");
define("LANG_NAMEFIELD_CAPTION", "Name");
define("LANG_PASSWORDFIELD_CAPTION", "Passwort");
define("LANG_SUBMITBUTTON", "Bestätigen");
define("LANG_DBCONNECTFAILED", "Problem beim Zugriff auf die Datenbank.");
define("LANG_LOGINSUCCESS", "Die Anmeldung war erfolgreich!");
define("LANG_LOGINFAILED", "Die Anmeldung war nicht erfolgreich!");
define("LANG_LINKCAPTION_FILES", "Dateien");
define("LANG_LINKCAPTION_CONTINUE", "Weiter");
define("LANG_BUTTON_LOGOUT", "Abmelden");
define("LANG_LINKCAPTION_RETRYLOGIN", "Erneut versuchen");
define("LANG_LINKCAPTION_ADMINUSERCREATE", "Neuen Benutzer anlegen");
define("LANG_LICENSE", "Lizenzierung");



?>
