<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/de/upload.lang.php
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */



define("LANG_PAGETITLE", "Hochladen");
define("LANG_HEADER", "Hochladen");
define("LANG_BUTTONCAPTION_SEND", "Absenden");
define("LANG_TEXT_FILENAME", "Vorgeschlagener Dateiname (leer lassen, um den Originalnamen beizubehalten)");
define("LANG_OPTIONCAPTION_DOWNLOADTYPEONCE", "einmalig");
define("LANG_OPTIONCAPTION_DOWNLOADTYPEALWAYS", "immer");
define("LANG_TEXT_SUBMITERROR", "Absenden fehlerhaft.");
define("LANG_TEXT_ERRORNUMBER", "Fehler aufgetreten mit Nummer ");
define("LANG_TEXT_FILETOOLARGE", "Die Datei hat mehr Bytes als ");
define("LANG_TEXT_ERRORFILEMOVE", "Die Datei wurde erfolgreich hochgeladen, konnte aber nicht abgelegt werden.");
define("LANG_TEXT_ERRORDBINSERTFILE", "Fehler beim Einfügen in die Datenbank.");
define("LANG_TEXT_SUCCESSUPLOAD", "Das Hochladen war erfolgreich!");
define("LANG_LINKCAPTION_UPLOADNEXT", "Weitere Datei hochladen");
define("LANG_LINKCAPTION_FILES", "Dateiübersicht");



?>
