<?php
/* Copyright (C) 2012-2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/upload.php
 * @brief Page for uploading new files.
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */


require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("upload"));
require_once("./libraries/file_management.inc.php");


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "  PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "  \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

if (isset($_POST['downloadtype']) !== true)
{
    echo "        <form enctype=\"multipart/form-data\" action=\"upload.php\" method=\"POST\">\n".
         "          <fieldset>\n".
         "            <input name=\"file\" type=\"file\" /><br />\n".
         "            <input name=\"name\" type=\"text\" size=\"40\" maxlength=\"255\" /> ".LANG_TEXT_FILENAME."<br />\n".
         "            <select name=\"downloadtype\">\n".
         "              <option value=\"".FILESETTING_DOWNLOADTYPE_ONCE."\">".LANG_OPTIONCAPTION_DOWNLOADTYPEONCE."</option>\n".
         "              <option value=\"".FILESETTING_DOWNLOADTYPE_ALWAYS."\">".LANG_OPTIONCAPTION_DOWNLOADTYPEALWAYS."</option>\n".
         "            </select><br />\n".
         "            <input type=\"submit\" value=\"".LANG_BUTTONCAPTION_SEND."\" /><br />\n".
         "          </fieldset>\n".
         "        </form>\n";
}
else
{
    $validFile = true;

    if (isset($_FILES['file']) !== true)
    {
        echo "        <p class=\"error\">".LANG_TEXT_SUBMITERROR."</p>\n";
        $validFile = false;
    }

    if ($validFile === true)
    {
        if ($_FILES['file']['error'] != 0)
        {
            echo "        <p class=\"error\">".LANG_TEXT_ERRORNUMBER.$_FILES['file']['error']."</p>\n";
            $validFile = false;
        }
    }

    if ($validFile === true)
    {
        /**
         * @todo In contrast to the hard upload_max_filesize and post_max_size, this is a
         *     soft-limit and needs to be made configurable.
         */
        /*
        if ($_FILES['file']['size'] > 10486000)
        {
            echo "        <p class=\"error\">".LANG_TEXT_FILETOOLARGE."10486000</p>\n";
            $validFile = false;
        }
        */
    }

    $id = null;

    if ($validFile === true)
    {
        for ($i = 0; $i < 30; $i++)
        {
            $tempId = md5(uniqid(rand(), true));

            if (file_exists("./files/".$tempId) === false)
            {
                $id = $tempId;
                break;
            }
        }

        if ($id == null)
        {
            echo "        <p class=\"error\">".LANG_TEXT_ERRORFILEMOVE."</p>\n";
            $validFile = false;
        }
    }

    if ($validFile === true)
    {
        if (move_uploaded_file($_FILES['file']['tmp_name'], "./files/".$id) === true)
        {
            $name = $_FILES['file']['name'];

            if (isset($_POST['name']) === true)
            {
                if (strlen($_POST['name']) > 0)
                {
                    $name = $_POST['name'];
                }
            }

            $result = InsertNewFile($id, $name, $_SESSION['user_id'], $_POST['downloadtype']);

            if ((int)$result > 0)
            {
                echo "        <p class=\"success\">".LANG_TEXT_SUCCESSUPLOAD."</p>\n".
                     "        <div>\n".
                     "          <a href=\"upload.php\">".LANG_LINKCAPTION_UPLOADNEXT."</a>\n".
                     "        </div>\n";
            }
            else
            {
                echo "        <p class=\"error\">".LANG_TEXT_ERRORDBINSERTFILE."</p>\n";
                $validFile = false;
            }
        }
        else
        {
            echo "        <p class=\"error\">".LANG_TEXT_ERRORFILEMOVE."</p>\n";
        }
    }
}

echo "        <div>\n".
     "          <a href=\"files.php\">".LANG_LINKCAPTION_FILES."</a>\n".
     "        </div>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";

?>
