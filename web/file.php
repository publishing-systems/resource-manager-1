<?php
/* Copyright (C) 2014-2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/file.php
 * @brief Endpoint for downloading.
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */



require_once("./libraries/https.inc.php");

if (isset($_GET['id']) !== true)
{
    header("HTTP/1.0 400 Bad Request");
    exit(0);
}

require_once("./libraries/file_management.inc.php");

$file = GetFileByHandle($_GET['id']);

if (is_array($file) !== true)
{
    header("HTTP/1.0 404 Not Found");
    exit(0);
}

$type = (int)$file['type'];

if ($type === FILESETTING_DOWNLOADTYPE_ONCE &&
    $file['datetime_downloaded'] != null)
{
    header("HTTP/1.0 404 Not Found");
    exit(0);
}

if ($type === FILESETTING_DOWNLOADTYPE_ONCE)
{
    // Do this first regardless if the download succeeds, so under no
    // condition can a file be requested more than once.
    if (SetFileDownloaded((int)$file['id']) !== 0)
    {
        header("HTTP/1.0 500 Internal Server Error");
        exit(0);
    }
}

$path = "./files/".$file['file'];

if (file_exists($path) !== true)
{
    // Don't tell the client that the handle is valid,
    // but just the actual file couldn't be found.
    header("HTTP/1.0 404 Not Found");
    exit(0);
}

//header("Content-type:application/zip");

/** @todo What about special characters in file names that need to be escaped here? */
header("Content-Disposition:attachment;filename=".$file['name']);

readfile($path);

if ($type === FILESETTING_DOWNLOADTYPE_ONCE)
{
    require_once("./libraries/user_management.inc.php");

    $user = GetUserById((int)$file['id_user']);

    if (is_array($user) === true)
    {
        @mail($user['e_mail'],
              "[Resource Manager] Download",
              "Name: ".@htmlspecialchars($file['name'], ENT_COMPAT | ENT_XHTML, "UTF-8")."\n".
              "Handle: ".@htmlspecialchars($_GET['id'], ENT_COMPAT | ENT_XHTML, "UTF-8")."\n".
              "Creation: ".$file['datetime_created']."Z\n".
              "Retrieval: ".gmdate("Y-m-d\TH:i:s")."Z\n",
              "From: ".$user['e_mail']."\n".
              "MIME-Version: 1.0\n".
              "Content-type: text/plain; charset=UTF-8\n");
    }
}

exit(0);

?>
