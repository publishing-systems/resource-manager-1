<?php
/* Copyright (C) 2012-2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/file_management.inc.php
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */



define("FILESETTING_DOWNLOADTYPE_ONCE", 1);
define("FILESETTING_DOWNLOADTYPE_ALWAYS", 2);



function InsertNewFile($id, $name, $userId, $downloadType)
{
    $downloadType = (int)$downloadType;

    if ($downloadType !== FILESETTING_DOWNLOADTYPE_ONCE &&
        $downloadType !== FILESETTING_DOWNLOADTYPE_ALWAYS)
    {
        return -1;
    }

    $userId = (int)$userId;
    /** @todo Maybe calculate SHA-256 of the file? Takes some time, but... */
    $handle = md5(uniqid(rand(), true));

    require_once(dirname(__FILE__)."/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        return -2;
    }

    $dbId = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."files` (`id`,\n".
                                    "    `file`,\n".
                                    "    `handle`,\n".
                                    "    `name`,\n".
                                    "    `type`,\n".
                                    "    `datetime_created`,\n".
                                    "    `datetime_downloaded`,\n".
                                    "    `id_user`)\n".
                                    "VALUES (?, ?, ?, ?, ?, UTC_TIMESTAMP(), ?, ?)\n",
                                    array(NULL, $id, $handle, $name, $downloadType, NULL, $userId),
                                    array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_NULL, Database::TYPE_INT));

    if ($dbId <= 0)
    {
        return -3;
    }

    return $dbId;
}

function GetFilesByUserId($userId)
{
    $userId = (int)$userId;

    require_once(dirname(__FILE__)."/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    $files = Database::Get()->Query("SELECT `id`,\n".
                                    "    `file`,\n".
                                    "    `handle`,\n".
                                    "    `name`,\n".
                                    "    `type`,\n".
                                    "    `datetime_created`,\n".
                                    "    `datetime_downloaded`\n".
                                    "FROM `".Database::Get()->GetPrefix()."files`\n".
                                    "WHERE `id_user`=?",
                                     array($userId),
                                     array(Database::TYPE_INT));

    if (is_array($files) !== true)
    {
        return -2;
    }

    return $files;
}

function GetFileByHandle($handle)
{
    require_once(dirname(__FILE__)."/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    $file = Database::Get()->Query("SELECT `id`,\n".
                                   "    `file`,\n".
                                   "    `name`,\n".
                                   "    `type`,\n".
                                   "    `datetime_created`,\n".
                                   "    `datetime_downloaded`,\n".
                                   "    `id_user`\n".
                                   "FROM `".Database::Get()->GetPrefix()."files`\n".
                                   "WHERE `handle` LIKE ?",
                                    array($handle),
                                    array(Database::TYPE_STRING));

    if (is_array($file) !== true)
    {
        return -2;
    }

    if (empty($file) == true)
    {
        return -3;
    }

    return $file[0];
}

function SetFileDownloaded($id)
{
    $id = (int)$id;

    require_once(dirname(__FILE__)."/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    if (Database::Get()->Execute("UPDATE `".Database::Get()->GetPrefix()."files`\n".
                                 "SET `datetime_downloaded`=UTC_TIMESTAMP()\n".
                                 "WHERE `id`=?",
                                 array($id),
                                 array(Database::TYPE_INT)) !== true)
    {
        return -2;
    }

    return 0;
}



?>
