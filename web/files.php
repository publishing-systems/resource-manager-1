<?php
/* Copyright (C) 2012-2019  Stephan Kreutzer
 *
 * This file is part of resource-manager-1.
 *
 * resource-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * resource-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with resource-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/files.php
 * @brief List of managed files.
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("files"));
require_once("./libraries/file_management.inc.php");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:tsorter=\"http://www.terrill.ca/sorting\" xsi:schemaLocation=\"http://w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "    <script type=\"text/javascript\" src=\"tsorter.js\"></script>\n".
     "    <script type=\"text/javascript\">\n".
     "      window.onload = function() {\n".
     "          tsorter.create('resources_table', 3);\n".
     "      };\n".
     "    </script>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n".
     "        <table id=\"resources_table\">\n".
     "          <thead>\n".
     "            <tr>\n".
     "              <th tsorter:data-tsorter=\"default\">".LANG_TABLECOLUMNCAPTION_FILENAME."</th>\n".
     "              <th tsorter:data-tsorter=\"link\">".LANG_TABLECOLUMNCAPTION_URL."</th>\n".
     "              <th tsorter:data-tsorter=\"default\">".LANG_TABLECOLUMNCAPTION_DOWNLOADTYPE."</th>\n".
     "              <th tsorter:data-tsorter=\"date\">".LANG_TABLECOLUMNCAPTION_CREATED."</th>\n".
     "              <th tsorter:data-tsorter=\"date\">".LANG_TABLECOLUMNCAPTION_DOWNLOADED."</th>\n".
     "            </tr>\n".
     "          </thead>\n".
     "          <tbody>\n";

$files = GetFilesByUserId($_SESSION['user_id']);

if (is_array($files) === true)
{
    foreach ($files as $file)
    {
        echo "            <tr>\n".
             "              <td>".htmlspecialchars($file['name'], ENT_XHTML, "UTF-8")."</td>\n".
             "              <td>".(HTTPS_ENABLED === true ? "https" : "http")."://".dirname($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']).DIRECTORY_SEPARATOR."file.php?id=".$file['handle']."</td>\n";

        switch ((int)$file['type'])
        {
        case FILESETTING_DOWNLOADTYPE_ONCE:
            echo "              <td>".LANG_TEXT_DOWNLOADTYPEONCE."</td>\n";
            break;
        case FILESETTING_DOWNLOADTYPE_ALWAYS:
            echo "              <td>".LANG_TEXT_DOWNLOADTYPEALWAYS."</td>\n";
            break;
        default:
            echo "              <td>?</td>\n";
            break;
        }

        echo "              <td>".$file['datetime_created']."</td>\n".
             "              <td>".$file['datetime_downloaded']."</td>\n".
             "            </tr>\n";
    }
}

echo "              </tbody>\n".
     "            </table>\n".
     "        <a href=\"upload.php\">".LANG_LINKCAPTION_UPLOAD."</a>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";



?>
